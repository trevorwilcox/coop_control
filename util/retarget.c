#include "retarget.h"
#include "uart.h"
#include <stdio.h>

void retarget_uart(void)
{
    uart_init();
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

int _write(int file, const char *p_char, int len)
{
    (void)file;

    return uart_write(p_char, len);
}

int _read(int file, char *p_char, int len)
{
    (void)file;

    return (uart_read(p_char, len) == 0) ? -1 : len;
}
