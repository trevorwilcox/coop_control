#include "tsl2561.h"
#include "shell.h"

#include <stdio.h>
#include <stdlib.h>

static void _whoami(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;
    uint8_t id_rev;

    if (tsl2561_whoami(&id_rev)) {
        printf("Failed! tsl2561_whoami\n");
    } else {
        printf("TSL2561 WhoAmI: %x\n", id_rev);
    }
}

static void _enable(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;

    if (tsl2561_enable()) {
        printf("Failed! tsl2561_enable\n");
    } else {
        printf("OK\n");
    }
}

static void _disable(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;

    if (tsl2561_disable()) {
        printf("Failed! tsl2561_disable\n");
    } else {
        printf("OK\n");
    }
}

static void _get_enable(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;
    bool en = false;

    if (tsl2561_get_enable(&en)) {
        printf("Failed! tsl2561_get_enable\n");
    } else {
        printf("TSL2561 %s.\n", en ? "enabled" : "disabled");
    }
}

static void _set_gain(uint8_t argc, char* argv[])
{
    if (argc != 2) {
        printf("usage: als_set_gain [0=low | 1=high]\n");
        return;
    }

    bool high_gain = (bool)(atoi(argv[1]) & 0x01);
    if (tsl2561_set_gain(high_gain)) {
        printf("Failed! tsl2561_set_gain\n");
    } else {
        printf("OK\n");
    }
}

static void _get_gain(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;
    bool gain;

    if (tsl2561_get_gain(&gain)) {
        printf("Failed! tsl2561_get_gain\n");
    } else {
        printf("TSL2561 gain: %s\n", gain ? "high" : "low");
    }
}

static void _set_int(uint8_t argc, char* argv[])
{
    if (argc != 2) {
        printf("usage: als_set_integration [0=13.7ms | 1=101ms | 2=402ms | 3=manual\n");
        return;
    }

    tsl2561_int_t new_int = (tsl2561_int_t)atoi(argv[1]);
    if (tsl2561_set_integration(new_int)) {
        printf("Failed! tsl2561_set_int\n");
    } else {
        printf("OK\n");
    }
}

static void _get_int(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;
    tsl2561_int_t integration;

    if (tsl2561_get_integration(&integration)) {
        printf("Failed! tsl2561_get_integration\n");
    } else {
        printf("TSL2561 integration: %d\n", (uint8_t)integration);
    }
}

static void _get_lux(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;

    uint32_t lux;

    if (tsl2561_get_lux(&lux)) {
        printf("Failed! tsl2561_get_lux\n");
    } else {
        printf("TSL2561 lux: %lu\n", lux);
    }
}

static void _ping(uint8_t argc, char* argv[])
{
    (void)argc;
    (void)argv;

    if (tsl2561_ping()) {
        printf("Failed! tsl2561_ping\n");
    } else {
        printf("OK\n");
    }
}

void tsl2561_shell_init(void)
{
    shell_cmd_t cmds[] = {
        {
            .desc = "als_whoami",
            .handler = _whoami
        },
        {
            .desc = "als_enable",
            .handler = _enable
        },
        {
            .desc = "als_disable",
            .handler = _disable
        },
        {
            .desc = "als_get_enable",
            .handler = _get_enable
        },
        {
            .desc = "als_set_gain",
            .handler = _set_gain
        },
        {
            .desc = "als_get_gain",
            .handler = _get_gain
        },
        {
            .desc = "als_set_integration",
            .handler = _set_int
        },
        {
            .desc = "als_get_integration",
            .handler = _get_int
        },
        {
            .desc = "als_get_lux",
            .handler = _get_lux
        },
        {
            .desc = "als_ping",
            .handler = _ping
        }
    };

    for (int i = 0; i < (sizeof(cmds) / sizeof(cmds[0])); i++) {
        shell_register_cmd(cmds[i]);
    }
}