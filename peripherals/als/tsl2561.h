#ifndef _TSL2561_H
#define _TSL2561_H

#include <stdint.h>
#include <stdbool.h>

#define TSL2561_PART_ID_REV           (0x1A)
#define TSL2561_PACKAGE_FN            (1)

#define TSL2561_ADDR                  (0x39)
#define TSL2561_SCL_PIN               (11)
#define TSL2561_SDA_PIN               (12)
#define TSL2561_INTERRUPT_PIN         (14)
#define TSL2561_TWI_INSTANCE_ID       0

#define TSL2561_CONTROL_REG           (0x00)
#define TSL2561_TIMING_REG            (0x01)
#define TSL2561_LOW_THRESH_LOWB_REG   (0x02)
#define TSL2561_LOW_THRESH_HIGHB_REG  (0x03)
#define TSL2561_HIGH_THRESH_LOWB_REG  (0x04)
#define TSL2561_HIGH_THRESH_HIGHB_REG (0x05)
#define TSL2561_INTERRUPT_REG         (0x06)
#define TSL2561_CRC_REG               (0x08)
#define TSL2561_ID_REG                (0x0A)
#define TSL2561_DATA0_LOWB_REG        (0x0C)
#define TSL2561_DATA0_HIGHB_REG       (0x0D)
#define TSL2561_DATA1_LOWB_REG        (0x0E)
#define TSL2561_DATA1_HIGHB_REG       (0x0F)

#define TSL2561_COMMAND_CMD_POS       (7)
#define TSL2561_COMMAND_CMD_MASK      (1 << TSL2561_COMMAND_CMD_POS)
#define TSL2561_COMMAND_WORD_POS      (5)
#define TSL2561_COMMAND_WORD_MASK     (1 << TSL2561_COMMAND_WORD_POS)
#define TSL2561_COMMAND_ADDR_POS      (0)
#define TSL2561_COMMAND_ADDR_MASK     (0x0F << TSL2561_COMMAND_ADDR_POS)

#define TSL2561_CONTROL_EN_POS        (0)
#define TSL2561_CONTROL_EN_MASK       (0x03 << TSL2561_CONTROL_EN_POS)

#define TSL2561_TIMING_GAIN_POS       (4)
#define TSL2561_TIMING_GAIN_MASK      (1 << TSL2561_TIMING_GAIN_POS)
#define TSL2561_TIMING_INT_POS        (0)
#define TSL2561_TIMING_INT_MASK       (0x03 << TSL2561_TIMING_INT_POS)

#define TSL2561_TIMING_INT_13_7_MS    (0x00)
#define TSL2561_TIMING_INT_101_MS     (0x01)
#define TSL2561_TIMING_INT_402_MS     (0x02)
#define TSL2561_TIMING_INT_MANUAL     (0x03)

#define TSL2561_LUX_LUXSCALE          (14)      // Scale by 2^14
#define TSL2561_LUX_RATIOSCALE        (9)       // Scale ratio by 2^9
#define TSL2561_LUX_CHSCALE           (10)      // Scale channel values by 2^10
#define TSL2561_LUX_CHSCALE_TINT0     (0x7517)  // 322/11 * 2^TSL2561_LUX_CHSCALE
#define TSL2561_LUX_CHSCALE_TINT1     (0x0FE7)  // 322/81 * 2^TSL2561_LUX_CHSCALE

// T, FN and CL package values    
#define TSL2561_LUX_K1T               (0x0040)  // 0.125 * 2^RATIO_SCALE
#define TSL2561_LUX_B1T               (0x01f2)  // 0.0304 * 2^LUX_SCALE
#define TSL2561_LUX_M1T               (0x01be)  // 0.0272 * 2^LUX_SCALE
#define TSL2561_LUX_K2T               (0x0080)  // 0.250 * 2^RATIO_SCALE
#define TSL2561_LUX_B2T               (0x0214)  // 0.0325 * 2^LUX_SCALE
#define TSL2561_LUX_M2T               (0x02d1)  // 0.0440 * 2^LUX_SCALE
#define TSL2561_LUX_K3T               (0x00c0)  // 0.375 * 2^RATIO_SCALE
#define TSL2561_LUX_B3T               (0x023f)  // 0.0351 * 2^LUX_SCALE
#define TSL2561_LUX_M3T               (0x037b)  // 0.0544 * 2^LUX_SCALE
#define TSL2561_LUX_K4T               (0x0100)  // 0.50 * 2^RATIO_SCALE
#define TSL2561_LUX_B4T               (0x0270)  // 0.0381 * 2^LUX_SCALE
#define TSL2561_LUX_M4T               (0x03fe)  // 0.0624 * 2^LUX_SCALE
#define TSL2561_LUX_K5T               (0x0138)  // 0.61 * 2^RATIO_SCALE
#define TSL2561_LUX_B5T               (0x016f)  // 0.0224 * 2^LUX_SCALE
#define TSL2561_LUX_M5T               (0x01fc)  // 0.0310 * 2^LUX_SCALE
#define TSL2561_LUX_K6T               (0x019a)  // 0.80 * 2^RATIO_SCALE
#define TSL2561_LUX_B6T               (0x00d2)  // 0.0128 * 2^LUX_SCALE
#define TSL2561_LUX_M6T               (0x00fb)  // 0.0153 * 2^LUX_SCALE
#define TSL2561_LUX_K7T               (0x029a)  // 1.3 * 2^RATIO_SCALE
#define TSL2561_LUX_B7T               (0x0018)  // 0.00146 * 2^LUX_SCALE
#define TSL2561_LUX_M7T               (0x0012)  // 0.00112 * 2^LUX_SCALE
#define TSL2561_LUX_K8T               (0x029a)  // 1.3 * 2^RATIO_SCALE
#define TSL2561_LUX_B8T               (0x0000)  // 0.000 * 2^LUX_SCALE
#define TSL2561_LUX_M8T               (0x0000)  // 0.000 * 2^LUX_SCALE

// CS package values    
#define TSL2561_LUX_K1C               (0x0043)  // 0.130 * 2^RATIO_SCALE
#define TSL2561_LUX_B1C               (0x0204)  // 0.0315 * 2^LUX_SCALE
#define TSL2561_LUX_M1C               (0x01ad)  // 0.0262 * 2^LUX_SCALE
#define TSL2561_LUX_K2C               (0x0085)  // 0.260 * 2^RATIO_SCALE
#define TSL2561_LUX_B2C               (0x0228)  // 0.0337 * 2^LUX_SCALE
#define TSL2561_LUX_M2C               (0x02c1)  // 0.0430 * 2^LUX_SCALE
#define TSL2561_LUX_K3C               (0x00c8)  // 0.390 * 2^RATIO_SCALE
#define TSL2561_LUX_B3C               (0x0253)  // 0.0363 * 2^LUX_SCALE
#define TSL2561_LUX_M3C               (0x0363)  // 0.0529 * 2^LUX_SCALE
#define TSL2561_LUX_K4C               (0x010a)  // 0.520 * 2^RATIO_SCALE
#define TSL2561_LUX_B4C               (0x0282)  // 0.0392 * 2^LUX_SCALE
#define TSL2561_LUX_M4C               (0x03df)  // 0.0605 * 2^LUX_SCALE
#define TSL2561_LUX_K5C               (0x014d)  // 0.65 * 2^RATIO_SCALE
#define TSL2561_LUX_B5C               (0x0177)  // 0.0229 * 2^LUX_SCALE
#define TSL2561_LUX_M5C               (0x01dd)  // 0.0291 * 2^LUX_SCALE
#define TSL2561_LUX_K6C               (0x019a)  // 0.80 * 2^RATIO_SCALE
#define TSL2561_LUX_B6C               (0x0101)  // 0.0157 * 2^LUX_SCALE
#define TSL2561_LUX_M6C               (0x0127)  // 0.0180 * 2^LUX_SCALE
#define TSL2561_LUX_K7C               (0x029a)  // 1.3 * 2^RATIO_SCALE
#define TSL2561_LUX_B7C               (0x0037)  // 0.00338 * 2^LUX_SCALE
#define TSL2561_LUX_M7C               (0x002b)  // 0.00260 * 2^LUX_SCALE
#define TSL2561_LUX_K8C               (0x029a)  // 1.3 * 2^RATIO_SCALE
#define TSL2561_LUX_B8C               (0x0000)  // 0.000 * 2^LUX_SCALE
#define TSL2561_LUX_M8C               (0x0000) // 0.000 * 2^LUX_SCALE

#define TSL2561_CLIPPING_13_7_MS      (4900)
#define TSL2561_CLIPPING_101_MS       (37000)
#define TSL2561_CLIPPING_402_MS       (65000)

typedef enum tsl2561_int_t {
    TSL2561_INT_13_7_MS = 0x00,
    TSL2561_INT_101_MS  = 0x01,
    TSL2561_INT_402_MS  = 0x02,
    TSL2561_INT_MANUAL  = 0x03
} tsl2561_int_t;

int tsl2561_init(void);

int tsl2561_enable(void);

int tsl2561_disable(void);

int tsl2561_get_enable(bool* p_en);

int tsl2561_set_gain(bool high_gain);

int tsl2561_get_gain(bool* p_high_gain);

int tsl2561_set_integration(tsl2561_int_t time);

int tsl2561_get_integration(tsl2561_int_t* p_time);

int tsl2561_get_lux(uint32_t* lux);

int tsl2561_whoami(uint8_t* p_id_rev);

int tsl2561_ping(void);

#endif //_TSL2561_H
