
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>

#include "tsl2561.h"

#include "nrf.h"
#include "nrf_drv_twi.h"
#include "app_util_platform.h"

static const nrf_drv_twi_t g_twi       = NRF_DRV_TWI_INSTANCE(TSL2561_TWI_INSTANCE_ID);
static tsl2561_int_t       g_int_time  = TSL2561_INT_402_MS;
static bool                g_high_gain = false;

static int _write8(uint8_t reg_addr, uint8_t value)
{
    uint8_t data[2];

    data[0] = TSL2561_COMMAND_CMD_MASK | (reg_addr & TSL2561_COMMAND_ADDR_MASK);
    data[1] = value;

    return nrf_drv_twi_tx(&g_twi, TSL2561_ADDR, &data[0], 2, false);
}

/*
static int _write16(uint8_t reg_addr, uint16_t value)
{
    uint8_t data[3];

    data[0] = TSL2561_COMMAND_CMD_MASK |
              TSL2561_COMMAND_WORD_MASK |
              (reg_addr & TSL2561_COMMAND_ADDR_MASK);
    data[1] = value & 0x0F;
    data[2] = (value & 0xF0) >> 8;

    return nrf_drv_twi_tx(&g_twi, TSL2561_ADDR, &(data[0]), 3, false);
}
*/

static int _read8(uint8_t reg_addr, uint8_t* p_value)
{
    uint8_t data = TSL2561_COMMAND_CMD_MASK | (reg_addr & TSL2561_COMMAND_ADDR_MASK);

    ret_code_t ret_val = nrf_drv_twi_tx(&g_twi, TSL2561_ADDR, &data, 1, false);

    if (ret_val) {
        return ret_val;
    }

    return nrf_drv_twi_rx(&g_twi, TSL2561_ADDR, (uint8_t*)p_value, 1);
}

static int _read16(uint8_t reg_addr, uint16_t* p_value)
{
    uint8_t data = TSL2561_COMMAND_CMD_MASK |
                   TSL2561_COMMAND_WORD_MASK |
                   (reg_addr & TSL2561_COMMAND_ADDR_MASK);

    ret_code_t ret_val = nrf_drv_twi_tx(&g_twi, TSL2561_ADDR, &data, 1, false);

    if (ret_val) {
        return ret_val;
    }

    return nrf_drv_twi_rx(&g_twi, TSL2561_ADDR, (uint8_t*)p_value, 2);
}

static uint32_t _calc_lux(uint16_t ch0_raw, uint16_t ch1_raw, tsl2561_int_t int_time, bool high_gain)
{
    unsigned long chScale;
    unsigned long channel1;
    unsigned long channel0;

    // Check for saturation
    uint16_t clipThreshold;
    switch (int_time) {
      case TSL2561_TIMING_INT_13_7_MS:
        clipThreshold = TSL2561_CLIPPING_13_7_MS;
        break;
      case TSL2561_TIMING_INT_101_MS:
        clipThreshold = TSL2561_CLIPPING_101_MS;
        break;
      case TSL2561_TIMING_INT_402_MS:
        // Fall through
      default:
        clipThreshold = TSL2561_CLIPPING_402_MS;
        break;
    }

    if ((ch0_raw > clipThreshold) || (ch1_raw > clipThreshold)) {
      return 65536;
    }

    // Get the correct scale depending on the intergration time
    switch (int_time) {
        case TSL2561_TIMING_INT_13_7_MS:
            chScale = TSL2561_LUX_CHSCALE_TINT0;
            break;
        case TSL2561_TIMING_INT_101_MS:
            chScale = TSL2561_LUX_CHSCALE_TINT1;
            break;
        case TSL2561_TIMING_INT_402_MS:
        // Fall through
        default:
            chScale = (1 << TSL2561_LUX_CHSCALE);
            break;
    }

    // Scale for gain (1x or 16x)
    if (!high_gain) {
        chScale = chScale << 4;
    }

    // Scale the channel values
    channel0 = (ch0_raw * chScale) >> TSL2561_LUX_CHSCALE;
    channel1 = (ch1_raw * chScale) >> TSL2561_LUX_CHSCALE;

    // Find the ratio of the channel values (Channel1/Channel0)
    unsigned long ratio1 = 0;
    if (channel0 != 0) {
        ratio1 = (channel1 << (TSL2561_LUX_RATIOSCALE+1)) / channel0;
    }

    // Round the ratio value
    unsigned long ratio = (ratio1 + 1) >> 1;
    unsigned int b, m;

#ifdef TSL2561_PACKAGE_CS
    if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1C)) {
        b = TSL2561_LUX_B1C; m = TSL2561_LUX_M1C;
    } else if (ratio <= TSL2561_LUX_K2C) {
        b = TSL2561_LUX_B2C; m = TSL2561_LUX_M2C;
    } else if (ratio <= TSL2561_LUX_K3C) {
        b = TSL2561_LUX_B3C; m = TSL2561_LUX_M3C;
    } else if (ratio <= TSL2561_LUX_K4C) {
        b = TSL2561_LUX_B4C; m = TSL2561_LUX_M4C;
    } else if (ratio <= TSL2561_LUX_K5C) {
        b = TSL2561_LUX_B5C; m = TSL2561_LUX_M5C;
    } else if (ratio <= TSL2561_LUX_K6C) {
        b = TSL2561_LUX_B6C; m = TSL2561_LUX_M6C;
    } else if (ratio <= TSL2561_LUX_K7C) {
        b = TSL2561_LUX_B7C; m = TSL2561_LUX_M7C;
    } else if (ratio > TSL2561_LUX_K8C) {
        b = TSL2561_LUX_B8C; m = TSL2561_LUX_M8C;
    } else {
        assert(0);
    }
#else
    if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1T)) {
        b = TSL2561_LUX_B1T; m = TSL2561_LUX_M1T;
    } else if (ratio <= TSL2561_LUX_K2T) {
        b = TSL2561_LUX_B2T; m = TSL2561_LUX_M2T;
    } else if (ratio <= TSL2561_LUX_K3T) {
        b = TSL2561_LUX_B3T; m = TSL2561_LUX_M3T;
    } else if (ratio <= TSL2561_LUX_K4T) {
        b = TSL2561_LUX_B4T; m = TSL2561_LUX_M4T;
    } else if (ratio <= TSL2561_LUX_K5T) {
        b = TSL2561_LUX_B5T; m = TSL2561_LUX_M5T;
    } else if (ratio <= TSL2561_LUX_K6T) {
        b = TSL2561_LUX_B6T; m = TSL2561_LUX_M6T;
    } else if (ratio <= TSL2561_LUX_K7T) {
        b = TSL2561_LUX_B7T; m = TSL2561_LUX_M7T;
    } else if (ratio > TSL2561_LUX_K8T) {
        b = TSL2561_LUX_B8T; m = TSL2561_LUX_M8T;
    } else {
        assert(0);
    }
#endif

    unsigned long temp;
    temp = ((channel0 * b) - (channel1 * m));
  
    if (temp < 0) {
        temp = 0;
    }
  
    // Round lsb (2^(LUX_SCALE-1))
    temp += (1 << (TSL2561_LUX_LUXSCALE - 1));
  
    // Strip fractional portion
    uint32_t lux = temp >> TSL2561_LUX_LUXSCALE;

    return lux;
}

int tsl2561_init(void)
{
    const nrf_drv_twi_config_t twi_tsl2561_config = {
       .scl                = TSL2561_SCL_PIN,
       .sda                = TSL2561_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    if (nrf_drv_twi_init(&g_twi, &twi_tsl2561_config, NULL, NULL)) {
        return -1;
    }

    nrf_drv_twi_enable(&g_twi);

    return tsl2561_ping();
}

int tsl2561_enable(void)
{
    return _write8(TSL2561_CONTROL_REG, TSL2561_CONTROL_EN_MASK);
}

int tsl2561_disable(void)
{
    return _write8(TSL2561_CONTROL_REG, 0);
}

int tsl2561_get_enable(bool* p_en)
{
    return _read8(TSL2561_CONTROL_REG, (uint8_t*)p_en);
}

int tsl2561_set_gain(bool high_gain)
{
    uint8_t prev_value;
    int ret_val;

    ret_val = _read8(TSL2561_TIMING_REG, &prev_value);
    if (ret_val) {
        return ret_val;
    }

    if (high_gain) {
        prev_value |= TSL2561_TIMING_GAIN_MASK;
    } else {
        prev_value &= ~TSL2561_TIMING_GAIN_MASK;
    }

    ret_val = _write8(TSL2561_TIMING_REG, prev_value);
    if (ret_val) {
        return ret_val;
    }

    g_high_gain = high_gain;

    return 0;
}

int tsl2561_get_gain(bool* p_high_gain)
{
    uint8_t reg_value;
    int ret_val;

    assert(p_high_gain);

    ret_val = _read8(TSL2561_TIMING_REG, &reg_value);
    if (ret_val) {
        return ret_val;
    }

    *p_high_gain = (bool)(reg_value & TSL2561_TIMING_GAIN_MASK);

    return 0;
}

int tsl2561_set_integration(tsl2561_int_t time)
{
    uint8_t prev_value;
    int ret_val;

    ret_val = _read8(TSL2561_TIMING_REG, &prev_value);
    if (ret_val) {
        return ret_val;
    }

    prev_value &= ~TSL2561_TIMING_INT_MASK;
    prev_value |= time & TSL2561_TIMING_INT_MASK;

    ret_val = _write8(TSL2561_TIMING_REG, prev_value & TSL2561_TIMING_INT_MASK);
    if (ret_val) {
        return ret_val;
    }

    g_int_time = time;

    return 0;
}

int tsl2561_get_integration(tsl2561_int_t* p_time)
{
    uint8_t reg_value;
    int ret_val;

    ret_val = _read8(TSL2561_TIMING_REG, &reg_value);
    if (ret_val) {
        return ret_val;
    }

    *p_time = (tsl2561_int_t)((reg_value & TSL2561_TIMING_INT_MASK) >> TSL2561_TIMING_INT_POS);

    return 0;
}

int tsl2561_get_lux(uint32_t* lux)
{
    uint16_t ch0_raw;
    uint16_t ch1_raw;
    int ret_val = 0;

    ret_val = _read16(TSL2561_DATA0_LOWB_REG, &ch0_raw);
    if (ret_val) {
        return ret_val;
    }

    ret_val = _read16(TSL2561_DATA1_LOWB_REG, &ch1_raw);
    if (ret_val) {
        return ret_val;
    }

    *lux = _calc_lux(ch0_raw, ch1_raw, g_int_time, g_high_gain);
    return 0;
}

int tsl2561_whoami(uint8_t* p_id_rev)
{
    return _read8(TSL2561_ID_REG, p_id_rev);
}

int tsl2561_ping(void)
{
    tsl2561_enable();

    uint8_t en_value;

    if (_read8(TSL2561_CONTROL_REG, &en_value)) {
        return -1;
    }

    if ((en_value & TSL2561_CONTROL_EN_MASK) != TSL2561_CONTROL_EN_MASK) {
        return -1;
    }

    tsl2561_disable();

    return 0;
}
