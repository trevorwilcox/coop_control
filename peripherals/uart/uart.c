#include "uart.h"
#include "nrf.h"
#include "bsp.h"
#include "app_uart.h"
#include "app_fifo.h"

#define UART_APP_FIFO_TX_SIZE 256
#define UART_APP_FIFO_RX_SIZE 256

static void _error_handler(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

int uart_init(void)
{
    uint32_t err_code;

    const app_uart_comm_params_t comm_params = {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_ENABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                  UART_APP_FIFO_RX_SIZE,
                  UART_APP_FIFO_TX_SIZE,
                  _error_handler,
                  APP_IRQ_PRIORITY_MID,
                  err_code);

    APP_ERROR_CHECK(err_code);

    return 0;
}

volatile int break_here;

int uart_read(void *p_data, uint32_t len)
{
    ASSERT(p_data);

    int i;

    for (i = 0; i < len; i++) {
        if (app_uart_get(&(((uint8_t*)p_data)[i])) != NRF_SUCCESS) {
            break;
        }
    }

    return i;
}

int uart_write(const void *p_data, uint32_t len)
{
    ASSERT(p_data);

    int i;

    for (i = 0; i < len; i++) {
        if (app_uart_put(((uint8_t*)p_data)[i]) != NRF_SUCCESS) {
            break;
        }
    }

    return i;
}