#ifndef _UART_H
#define _UART_H

#include <stdint.h>

int uart_init(void);

int uart_read(void *p_data, uint32_t len);

int uart_write(const void *p_data, uint32_t len);


#endif //_UART_H